<!--
SPDX-FileCopyrightText: 2024 Michael Picht <mipi@fsfe.org>

SPDX-License-Identifier: GPL-3.0-or-later
-->

# Contributing to print_logger

**Contributions to print_logger are welcome!**

## Errors, feature requests, etc.

In case of problems, questions, feature requests, or other kinds of suggestions or feedback, please create an issue in the [print_logger repository](https://gitlab.com/rust-utils/print_logger).

## Bug fixes and new features

To contribute bug fixes or new features, please open a pull request in the [print_logger repository](https://gitlab.com/rust-utils/print_logger).
