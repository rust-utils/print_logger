<!--
SPDX-FileCopyrightText: 2024 Michael Picht <mipi@fsfe.org>

SPDX-License-Identifier: GPL-3.0-or-later
-->

[![Crates.io (latest)](https://img.shields.io/crates/dv/print_logger?label=crates.io&style=flat)](https://crates.io/crates/print_logger)
[![Documentation](https://docs.rs/print_logger/badge.svg)](https://docs.rs/print_logger)
[![REUSE status](https://api.reuse.software/badge/gitlab.com/rust-utils/print_logger)](https://api.reuse.software/info/gitlab.com/rust-utils/print_logger)

# print_logger

A simple logger that prints messages either to stdout or stderr (depending on the message type). A fixed color scheme is applied.

## Simple example

	use log::*;
	use regex::Regex;

	// ...

	print_logger::new()
		.targets_by_regex(&[Regex::new(&format!("^{}[::.+]*", module_path!())).unwrap()])
		.level_filter(LevelFilter::Info)
		.init()
		.unwrap();

	error!("some failure");

	// ...

### Module Level Logging

`print_logger` offers the possibility to restrict components which can log. Many crates use [log](https://docs.rs/log/*/log/) but you may not want their output in your application. For example [hyper](https://docs.rs/hyper/*/hyper/) makes heavy use of log but when your application receives `-vvvvv` to enable the `trace!()` messages you don't want the output of `hyper`'s `trace!()` level.

To support this `print_logger` includes two methods:

1. With `targets_by_name` a list of log targets (see <https://docs.rs/log/latest/log/macro.error.html>) can be specified. Only messages for these targets are displayed.

2. With `targets_by_regex` a list of regular expressions can be specified. Messages are only displayed if their target matches at least one of these expressions. In the example above only messages from the binary itself but none of its dependencies are displayed (per default the current module is used as target in log messages).

If both lists are given, a message is displayed if its target either is in `targets_by_name` or if it matches one to the expressions of `targets_by_regex`. Target names (i.e., per default the actual module path) are displayed at the beginning of the log message if the `print_target_filter` is greater or equal than the specified `level_filter`.

# License

[GNU Public License v3.0](https://gitlab.com/rust-utils/print_logger/blob/main/LICENSE)
