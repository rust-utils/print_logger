<!--
SPDX-FileCopyrightText: 2024 Michael Picht <mipi@fsfe.org>

SPDX-License-Identifier: GPL-3.0-or-later
-->

# Changelog

## [Release 0.2.0](https://gitlab.com/rust-utils/print_logger/tags/v0.2.0) (2024-02-08)

### Changed

- Moved from [colored](https://docs.rs/colored/) to [termcolor](https://docs.rs/termcolor/)

## [Release 0.1.0](https://gitlab.com/rust-utils/print_logger/tags/v0.1.0) (2024-01-27)

Initial version
