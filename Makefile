# SPDX-FileCopyrightText: 2024 Michael Picht <mipi@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Makefile is only used to create a new release. Usage:
# 	make RELEASE=X.Y.Z

# (1) adjust version in Cargo.toml, commit and push changes
# (2) create an annotated tag with name RELEASE and push change
# (3) publish new version to crate.io
all:
	@if [ -z $(RELEASE) ]; then \
		echo "no new release submitted"; \
		exit 1; \
	fi	
	@VER_NEW=$(RELEASE); \
	VER_OLD=`sed -n "s/^version *= \"*\(.*\)\"/\1/p" ./Cargo.toml`; \
	if ! [ $$((`vercmp $${VER_OLD} $${VER_NEW}`)) -lt 0 ]; then \
		echo "new version is not greater than old version"; \
		exit 1; \
	fi; \
	sed -i -e "s/^version.*/version = \"$${VER_NEW}\"/" ./Cargo.toml;
	@git commit -a -s -m "release $(RELEASE)"
	@git push
	@git tag -a $(RELEASE) -m "release $(RELEASE)"
	@git push origin $(RELEASE)
